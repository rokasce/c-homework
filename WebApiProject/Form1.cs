﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Newtonsoft.Json;
using ToolsLibrary;

namespace WebApiProject
{
    public partial class Form1 : Form
    {
        private ToolsClass tools = new ToolsClass();
        private string SearchBy = "name";

        public Form1()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            //Valiuta
            //var restCountries = new RestCountries();
            //foreach (var country in restCountries.FindByCurr(textBox1.Text))
            //{ ... }
            ClearComboBox();
            if (textBox1.Text.Length > 2)
            {
                var countryList = await tools.GetCountries(SearchBy, textBox1.Text);
                if (countryList != null)
                {
                    if (LanguageRadioButton.Checked || CurrencyRadioButton.Checked)
                    {
                        comboBox1.Text = $"Results ({countryList.Count})";
                        foreach (var country in countryList)
                        {
                            comboBox1.Items.Add(country.name);
                        }
                    }
                    else
                    {
                        PopulateFields(countryList);
                    }
                }
                else
                {
                    MessageBox.Show("No results, change the input");
                }
                textBox1.Text = "";
            }
            else
            {
                MessageBox.Show("Input must be longer then 2 characters.");
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            SearchBy = "name";
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            SearchBy = "alpha";
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            SearchBy = "capital";
        }

        private void ClearTextButton_Click(object sender, EventArgs e)
        {
            CountryNameTextBox.Text = "";
            AlphaCodeTextBox.Text = "";
            CapitalTextBox.Text = "";
            PopulationTextBox.Text = "";
            RegionTextBox.Text = "";
            SubregionTextBox.Text = "";
            NativeNameTextBox.Text = "";
            NumericCodeTextBox.Text = "";
            CurrenciesTextBox.Text = "";
            LanguagesTextBox.Text = "";
            ClearComboBox();
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            SearchBy = "currency";
            ClearComboBox();
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            SearchBy = "lang";
            ClearComboBox();
        }

        private async void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedCountry = comboBox1.SelectedItem.ToString();
            var temp = SearchBy;
            var country = await tools.GetCountries(SearchBy = "name", selectedCountry);
            SearchBy = temp;
            PopulateFields(country);

        }

        public void PopulateFields(List<Country> countryList)
        {
            foreach (var country in countryList)
            {
                CountryNameTextBox.Text = country.name;
                AlphaCodeTextBox.Text = country.alpha3Code;
                CapitalTextBox.Text = country.capital;
                PopulationTextBox.Text = country.population.ToString();
                RegionTextBox.Text = country.region;
                SubregionTextBox.Text = country.subregion;
                NativeNameTextBox.Text = country.nativeName;
                NumericCodeTextBox.Text = country.numericCode;
                CurrenciesTextBox.Text = "";
                foreach (var c in country.currencies)
                {
                    CurrenciesTextBox.Text += $"{c.name}({c.symbol}) ";
                }

                LanguagesTextBox.Text = "";
                foreach (var l in country.Languages)
                {
                    LanguagesTextBox.Text += $"{l.name} ";
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ClearComboBox();
        }

        private void ClearComboBox()
        {
            comboBox1.Items.Clear();
            comboBox1.Text = "Results (0)";
        }
    }
}