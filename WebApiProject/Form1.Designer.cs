﻿namespace WebApiProject
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.CountryNameTextBox = new System.Windows.Forms.TextBox();
            this.AlphaCodeTextBox = new System.Windows.Forms.TextBox();
            this.CapitalTextBox = new System.Windows.Forms.TextBox();
            this.PopulationTextBox = new System.Windows.Forms.TextBox();
            this.ClearTextButton = new System.Windows.Forms.Button();
            this.RegionTextBox = new System.Windows.Forms.TextBox();
            this.SubregionTextBox = new System.Windows.Forms.TextBox();
            this.NativeNameTextBox = new System.Windows.Forms.TextBox();
            this.NumericCodeTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.CurrencyRadioButton = new System.Windows.Forms.RadioButton();
            this.LanguageRadioButton = new System.Windows.Forms.RadioButton();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.CurrenciesTextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.LanguagesTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(123, 147);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(28, 53);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(53, 17);
            this.radioButton1.TabIndex = 3;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Name";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(87, 53);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(83, 17);
            this.radioButton2.TabIndex = 4;
            this.radioButton2.Text = "Alpha3Code";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(176, 53);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(57, 17);
            this.radioButton3.TabIndex = 5;
            this.radioButton3.Text = "Capital";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Search by:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(256, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Enter name/alpha3code/capital/language/currency:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(28, 107);
            this.textBox1.MaxLength = 40;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(267, 20);
            this.textBox1.TabIndex = 8;
            // 
            // CountryNameTextBox
            // 
            this.CountryNameTextBox.Location = new System.Drawing.Point(478, 70);
            this.CountryNameTextBox.Name = "CountryNameTextBox";
            this.CountryNameTextBox.ReadOnly = true;
            this.CountryNameTextBox.Size = new System.Drawing.Size(228, 20);
            this.CountryNameTextBox.TabIndex = 9;
            // 
            // AlphaCodeTextBox
            // 
            this.AlphaCodeTextBox.Location = new System.Drawing.Point(478, 107);
            this.AlphaCodeTextBox.Name = "AlphaCodeTextBox";
            this.AlphaCodeTextBox.ReadOnly = true;
            this.AlphaCodeTextBox.Size = new System.Drawing.Size(228, 20);
            this.AlphaCodeTextBox.TabIndex = 10;
            // 
            // CapitalTextBox
            // 
            this.CapitalTextBox.Location = new System.Drawing.Point(478, 150);
            this.CapitalTextBox.Name = "CapitalTextBox";
            this.CapitalTextBox.ReadOnly = true;
            this.CapitalTextBox.Size = new System.Drawing.Size(228, 20);
            this.CapitalTextBox.TabIndex = 11;
            // 
            // PopulationTextBox
            // 
            this.PopulationTextBox.Location = new System.Drawing.Point(478, 187);
            this.PopulationTextBox.Name = "PopulationTextBox";
            this.PopulationTextBox.ReadOnly = true;
            this.PopulationTextBox.Size = new System.Drawing.Size(228, 20);
            this.PopulationTextBox.TabIndex = 12;
            // 
            // ClearTextButton
            // 
            this.ClearTextButton.Location = new System.Drawing.Point(563, 468);
            this.ClearTextButton.Name = "ClearTextButton";
            this.ClearTextButton.Size = new System.Drawing.Size(75, 23);
            this.ClearTextButton.TabIndex = 13;
            this.ClearTextButton.Text = "Clear";
            this.ClearTextButton.UseVisualStyleBackColor = true;
            this.ClearTextButton.Click += new System.EventHandler(this.ClearTextButton_Click);
            // 
            // RegionTextBox
            // 
            this.RegionTextBox.Location = new System.Drawing.Point(478, 228);
            this.RegionTextBox.Name = "RegionTextBox";
            this.RegionTextBox.ReadOnly = true;
            this.RegionTextBox.Size = new System.Drawing.Size(228, 20);
            this.RegionTextBox.TabIndex = 14;
            // 
            // SubregionTextBox
            // 
            this.SubregionTextBox.Location = new System.Drawing.Point(478, 267);
            this.SubregionTextBox.Name = "SubregionTextBox";
            this.SubregionTextBox.ReadOnly = true;
            this.SubregionTextBox.Size = new System.Drawing.Size(228, 20);
            this.SubregionTextBox.TabIndex = 15;
            // 
            // NativeNameTextBox
            // 
            this.NativeNameTextBox.Location = new System.Drawing.Point(478, 306);
            this.NativeNameTextBox.Name = "NativeNameTextBox";
            this.NativeNameTextBox.ReadOnly = true;
            this.NativeNameTextBox.Size = new System.Drawing.Size(228, 20);
            this.NativeNameTextBox.TabIndex = 16;
            // 
            // NumericCodeTextBox
            // 
            this.NumericCodeTextBox.Location = new System.Drawing.Point(478, 345);
            this.NumericCodeTextBox.Name = "NumericCodeTextBox";
            this.NumericCodeTextBox.ReadOnly = true;
            this.NumericCodeTextBox.Size = new System.Drawing.Size(228, 20);
            this.NumericCodeTextBox.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(541, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Country information";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(475, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Country name:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(475, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Alpha code:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(475, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Capital city:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(475, 171);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Population:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(475, 212);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Region:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(475, 251);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Subregion:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(475, 290);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "Native name:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(475, 329);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Country code:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(28, 204);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(267, 21);
            this.comboBox1.TabIndex = 27;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // CurrencyRadioButton
            // 
            this.CurrencyRadioButton.AutoSize = true;
            this.CurrencyRadioButton.Location = new System.Drawing.Point(318, 53);
            this.CurrencyRadioButton.Name = "CurrencyRadioButton";
            this.CurrencyRadioButton.Size = new System.Drawing.Size(67, 17);
            this.CurrencyRadioButton.TabIndex = 29;
            this.CurrencyRadioButton.TabStop = true;
            this.CurrencyRadioButton.Text = "Currency";
            this.CurrencyRadioButton.UseVisualStyleBackColor = true;
            this.CurrencyRadioButton.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // LanguageRadioButton
            // 
            this.LanguageRadioButton.AutoSize = true;
            this.LanguageRadioButton.Location = new System.Drawing.Point(239, 53);
            this.LanguageRadioButton.Name = "LanguageRadioButton";
            this.LanguageRadioButton.Size = new System.Drawing.Size(73, 17);
            this.LanguageRadioButton.TabIndex = 30;
            this.LanguageRadioButton.TabStop = true;
            this.LanguageRadioButton.Text = "Language";
            this.LanguageRadioButton.UseVisualStyleBackColor = true;
            this.LanguageRadioButton.CheckedChanged += new System.EventHandler(this.radioButton5_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(25, 188);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(204, 13);
            this.label12.TabIndex = 31;
            this.label12.Text = "Results for language and currency option:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(475, 370);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(57, 13);
            this.label13.TabIndex = 33;
            this.label13.Text = "Currencies";
            // 
            // CurrenciesTextBox
            // 
            this.CurrenciesTextBox.Location = new System.Drawing.Point(478, 386);
            this.CurrenciesTextBox.Name = "CurrenciesTextBox";
            this.CurrenciesTextBox.ReadOnly = true;
            this.CurrenciesTextBox.Size = new System.Drawing.Size(228, 20);
            this.CurrenciesTextBox.TabIndex = 32;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(475, 410);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 13);
            this.label14.TabIndex = 35;
            this.label14.Text = "Languages:";
            // 
            // LanguagesTextBox
            // 
            this.LanguagesTextBox.Location = new System.Drawing.Point(478, 426);
            this.LanguagesTextBox.Name = "LanguagesTextBox";
            this.LanguagesTextBox.ReadOnly = true;
            this.LanguagesTextBox.Size = new System.Drawing.Size(228, 20);
            this.LanguagesTextBox.TabIndex = 34;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(785, 527);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.LanguagesTextBox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.CurrenciesTextBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.LanguageRadioButton);
            this.Controls.Add(this.CurrencyRadioButton);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.NumericCodeTextBox);
            this.Controls.Add(this.NativeNameTextBox);
            this.Controls.Add(this.SubregionTextBox);
            this.Controls.Add(this.RegionTextBox);
            this.Controls.Add(this.ClearTextButton);
            this.Controls.Add(this.PopulationTextBox);
            this.Controls.Add(this.CapitalTextBox);
            this.Controls.Add(this.AlphaCodeTextBox);
            this.Controls.Add(this.CountryNameTextBox);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox CountryNameTextBox;
        private System.Windows.Forms.TextBox AlphaCodeTextBox;
        private System.Windows.Forms.TextBox CapitalTextBox;
        private System.Windows.Forms.TextBox PopulationTextBox;
        private System.Windows.Forms.Button ClearTextButton;
        private System.Windows.Forms.TextBox RegionTextBox;
        private System.Windows.Forms.TextBox SubregionTextBox;
        private System.Windows.Forms.TextBox NativeNameTextBox;
        private System.Windows.Forms.TextBox NumericCodeTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.RadioButton CurrencyRadioButton;
        private System.Windows.Forms.RadioButton LanguageRadioButton;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox CurrenciesTextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox LanguagesTextBox;
    }
}

