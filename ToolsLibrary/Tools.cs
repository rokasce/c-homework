﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ToolsLibrary
{
    public class ToolsClass
    {
        public static HttpClient client = new HttpClient();

        public async Task<List<Country>> GetCountries(string searchBy, string param)
        {
            var path = "https://restcountries.eu/rest/v2";
            path = path + $"/{searchBy}" + $"/{param}";

            string json = String.Empty;

            var response = await client.GetAsync(path);

            List<Country> temp = null;
            if (response.IsSuccessStatusCode)
            {
                json = await response.Content.ReadAsStringAsync();

                if (json.StartsWith("["))
                {
                    try
                    {
                        temp = JsonConvert.DeserializeObject<List<Country>>(json);
                    }
                    catch (Exception e)
                    {
                        temp = null;
                    }
                }
                else
                {
                    try
                    {
                        json = "[" + json + "]";
                        temp = JsonConvert.DeserializeObject<List<Country>>(json);
                    }
                    catch (Exception e)
                    {
                        temp = null;
                    }
                }
            }
            else
            {
                temp = null;
            }
            return temp;
        }
    }

    public class Country
    {
        public string name { get; set; }
        public string alpha3Code { get; set; }
        public string capital { get; set; }
        public string region { get; set; }
        public string subregion { get; set; }
        public int population { get; set; }
        public string nativeName { get; set; }
        public string numericCode { get; set; }
        public List<Currency> currencies  { get; set; }
        public List<Language> Languages { get; set; }
    }

    public class Currency
    {
        public string code { get; set; }
        public string name { get; set; }
        public string symbol { get; set; }
    }

    public class Language
    {
        public string name { get; set; }
        public string nativeName { get; set; }
    }
}
