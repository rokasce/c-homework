﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using ToolsLibrary;

namespace Tools.UnitTests
{
    [TestClass]
    public class ToolsUnitTests
    {
        [TestMethod]
        public async Task GetCountriesList_InvalidRequest_ReturnsNull()
        {
            //Arrange
            var tools = new ToolsClass();

            //Act
            var result = await tools.GetCountries("RandomStuff", "EvenMoreRandom");

            //Assert
            Assert.IsNull(result);
        }

        [TestMethod]
        public async Task GetCountriesList_EqualObjects_ReturnsCountryList()
        {
            //Arrange
            var tools = new ToolsClass();

            //Act
            var result = await tools.GetCountries("name", "lithuania");

            var testJson =
                @"[{'name':'Lithuania','topLevelDomain':['.lt'],'alpha2Code':'LT','alpha3Code':'LTU','callingCodes':['370'],'capital':'Vilnius','altSpellings':['LT','Republic of Lithuania','Lietuvos Respublika'],'region':'Europe','subregion':'Northern Europe','population':2872294,'latlng':[56.0,24.0],'demonym':'Lithuanian','area':65300.0,'gini':37.6,'timezones':['UTC+02:00'],'borders':['BLR','LVA','POL','RUS'],'nativeName':'Lietuva','numericCode':'440','currencies':[{'code':'EUR','name':'Euro','symbol':'?'}],'languages':[{'iso639_1':'lt','iso639_2':'lit','name':'Lithuanian','nativeName':'lietuvių kalba'}],'translations':{'de':'Litauen','es':'Lituania','fr':'Lituanie','ja':'?????','it':'Lituania','br':'Litu?nia','pt':'Litu?nia','nl':'Litouwen','hr':'Litva','fa':'???????'},'flag':'https://restcountries.eu/data/ltu.svg','regionalBlocs':[{'acronym':'EU','name':'European Union','otherAcronyms':[],'otherNames':[]}],'cioc':'LTU'}]";
            var testObject = JsonConvert.DeserializeObject<List<Country>>(testJson);


            //Assert
            Assert.AreEqual(testObject.GetType(), result.GetType());
        }
    }
}
